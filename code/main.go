package main

import (
	"fmt"

	"./ch4"
	"./ch5"
	"./ch6"
	"./ch7"
	"./ch8"
	"./ch9"
)

func main() {
	fmt.Println("===== [ch4] =====")
	fmt.Println("\n// variables.go")
	ch4.VariablesCall()
	fmt.Println("\n// functions.go")
	ch4.FunctionsCall()

	fmt.Println("\n===== [ch5] =====")
	fmt.Println("\n// structures.go")
	ch5.StructuresCall()
	fmt.Println("\n// composition.go")
	ch5.CompositionCall()
	fmt.Println("\n// overloading.go")
	ch5.OverloadingCall()

	fmt.Println("\n===== [ch6] =====")
	fmt.Println("\n// arrays.go")
	ch6.ArraysCall()
	fmt.Println("\n// slices.go")
	ch6.SlicesCall()
	fmt.Println("\n// maps.go")
	ch6.MapsCall()

	fmt.Println("\n===== [ch7] =====")
	fmt.Println("\n// packages.go")
	fmt.Println("\n// interfaces.go")
	ch7.InterfacesCall()

	fmt.Println("\n===== [ch8] =====")
	fmt.Println("\n// errors.go")
	ch8.ErrorsCall()
	fmt.Println("\n// defers.go")
	ch8.DefersCall()
	fmt.Println("\n// initialized_if.go")
	ch8.InitializedIfCall()
	fmt.Println("\n// initialized_if.go")
	ch8.EmptyInterfacesCall()

	fmt.Println("\n===== [ch9] =====")
	fmt.Println("\n// goroutines.go")
	ch9.GoroutinesCall()
	fmt.Println("\n// syncronization.go")
	ch9.SyncronizationCall()
	fmt.Println("\n// channels.go")
	ch9.ChannelsCall()
}
