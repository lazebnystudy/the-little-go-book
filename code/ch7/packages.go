package ch7

type Item struct {
	Price float64
}

func LoadItem(id int) *Item {
	return &Item{
		Price: 100.5,
	}
}

func PriceCheck(itemId int) (float64, bool) {
	item := LoadItem(itemId)
	if item == nil {
		return 0, false
	}
	return item.Price, true
}
