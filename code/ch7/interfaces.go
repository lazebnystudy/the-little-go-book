package ch7

import "fmt"

func InterfacesCall() {
	server := &Server{
		logger: &ConsoleLogger{},
	}
	server.process()
}

type Logger interface {
	Log(message string)
}

type ConsoleLogger struct{}

type Server struct {
	logger Logger
}

func (l ConsoleLogger) Log(message string) {
	fmt.Printf("STDOUT logger: %s\n", message)
}

func (server *Server) process() {
	server.logger.Log("Process started")
}
