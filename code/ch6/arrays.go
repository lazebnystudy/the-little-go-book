package ch6

import (
	"fmt"
)

func ArraysCall() {
	var scores1 [10]int
	scores1[0] = 339

	scores2 := [4]int{10, 20, 30, 40}
	for index, value := range scores2 {
		fmt.Println(index, value)
	}
}
