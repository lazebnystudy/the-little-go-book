package ch6

import "fmt"

func MapsCall() {
	lookup := make(map[string]int)
	lookup["goku"] = 9001
	power, exists := lookup["vegeta"]
	fmt.Println(power, exists)

	total := len(lookup)   // get map length
	delete(lookup, "goku") // delete a value from a map
	fmt.Println(total, lookup)

	lookup = make(map[string]int, 100) // define a map with cap 100

	simpleWayCreateAMap()
	defineAStructureWithMap()
	iterationOverMap()
}

type Saiyan2 struct {
	Name    string
	Friends map[string]*Saiyan2
}

func simpleWayCreateAMap() map[string]int {
	return map[string]int{
		"goku":    9001,
		"krillin": 2044,
	}
}

func defineAStructureWithMap() {
	goku := &Saiyan2{
		Name:    "goku",
		Friends: make(map[string]*Saiyan2),
	}
	goku.Friends["krillin"] = &Saiyan2{Name: "Krillin"}
	fmt.Println(goku)
}

func iterationOverMap() {
	for key, value := range simpleWayCreateAMap() { // keys aren't ordered
		fmt.Println(key, value)
	}
}
