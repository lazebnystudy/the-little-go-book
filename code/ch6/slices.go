package ch6

import "fmt"

func SlicesCall() {
	// simple way create a slice
	score1 := []int{10, 20, 30}
	fmt.Println(score1)

	// create a slice with len 10 and capacity 10
	score2 := make([]int, 10)
	score2[7] = 20
	fmt.Println(score2)

	// Add element to slice
	score3 := make([]int, 0, 10) // len = 0, capacity = 10
	// score3[5] = 20 // raise an error
	score3 = append(score3, 20)
	fmt.Println(score3)

	// Reslice a slice (we can reslice a slice up to it capacity)
	score4 := make([]int, 0, 10)
	score4 = score4[0:8]
	score4[7] = 20
	fmt.Println(score4)

	fmt.Println("Resize a slice")
	resizeASlice()
	// 4 ways to initialize a slice
	initASliceWhenWeKnowAValues()
	initASliceWhenWeWriteToSpecificIndex([]*Saiyan{&Saiyan{Power: 10}})
	initASliceWithUnlimitedCountOfElements([]*Saiyan{&Saiyan{Power: 10}})
	initASliceWithSubArray()
}

func resizeASlice() {
	xs := make([]int, 0, 5)
	c := cap(xs)

	printCapacity(xs)

	for i := 0; i < 25; i++ {
		xs = append(xs, i)

		if cap(xs) != c {
			c = cap(xs)
			printCapacity(xs)
		}
	}
}

func printCapacity(xs []int) {
	fmt.Printf("Cap is: %d\n", cap(xs))
}

func initASliceWhenWeKnowAValues() []string {
	return []string{"leto", "jesica", "paul"}
}

type Saiyan struct {
	Power int
}

func initASliceWhenWeWriteToSpecificIndex(saiyans []*Saiyan) []int {
	powers := make([]int, len(saiyans))
	for index, saiyan := range saiyans {
		powers[index] = saiyan.Power
	}
	return powers
}

func initASliceWithUnlimitedCountOfElements(saiyans []*Saiyan) []int {
	powers := make([]int, 0, len(saiyans))
	for _, saiyan := range saiyans {
		powers = append(powers, saiyan.Power)
	}
	return powers
}

func initASliceWithSubArray() {
	scores := []int{10, 20, 30, 40, 50}
	fmt.Println(scores)
	scores2 := scores[:3]
	scores3 := scores[3:]
	scores4 := scores[1:3]
	scores4[1] = 100 // change value in srores4 and in socres
	fmt.Println(scores2)
	fmt.Println(scores3)
	fmt.Println(scores4)
	fmt.Println(scores)
}
