package ch8

import (
	"fmt"
)

func DefersCall() {
	fmt.Println("Open file")
	defer fmt.Println("Close file")
	fmt.Println("Work with file")
}
