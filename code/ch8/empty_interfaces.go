package ch8

import "fmt"

func EmptyInterfacesCall() {
	fmt.Println(emptyInterfacesAdd(5, 6))
	fmt.Println(emptyInterfacesAddWithSwitch(5, 6))
	fmt.Println(emptyInterfacesAddWithSwitch(true, true))
	fmt.Println(emptyInterfacesAddWithSwitch(true, false))

}

func emptyInterfacesAdd(a interface{}, b interface{}) int {
	return a.(int) + b.(int) // convert types to int
}

// ugly style
func emptyInterfacesAddWithSwitch(a interface{}, b interface{}) interface{} {
	switch a.(type) {
	case int:
		return a.(int) + b.(int)
	case bool:
		return a.(bool) && b.(bool)
	default:
	}
	return nil
}
