package ch8

import (
	"errors"
	"fmt"
)

func InitializedIfCall() {
	if err := initializedProcess(-10); err != nil {
		fmt.Println(err)
	}
}

func initializedProcess(count int) error {
	if count < 0 {
		return errors.New("Count is wrong")
	}
	return nil
}
