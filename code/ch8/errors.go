package ch8

import (
	"errors"
	"fmt"
	"io"
)

func ErrorsCall() {
	fmt.Println(errorsProcess(0))
	fmt.Println(errorsProcess(1))
	// errorsScan()
}

func errorsProcess(count int) error {
	if count < 1 {
		return errors.New("Invalid count")
	}

	return nil
}

func errorsScan() {
	var input int
	_, err := fmt.Scan(&input)
	if err == io.EOF {
		fmt.Println("no more input!")
	}
}
