package ch9

import (
	"fmt"
	"sync"
	"time"
)

var (
	counter = 0
	lock    sync.Mutex
)

func SyncronizationCall() {
	syncronizationMutexCall()
}

func syncronizationMutexCall() {
	for i := 1; i < 20; i++ {
		go syncronizationMutexInc()
	}
	time.Sleep(time.Millisecond * 10)
}

func syncronizationMutexInc() {
	lock.Lock()
	defer lock.Unlock()

	counter++
	fmt.Println(counter)
}
