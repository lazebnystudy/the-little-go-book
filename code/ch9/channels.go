package ch9

import (
	"fmt"
	"math/rand"
	"time"
)

type ChannelsWorker struct {
	id int
}

func ChannelsCall() {
	channelsBufferExample()
	channelsSelectExample()
}

func channelsBufferExample() {
	c := make(chan int, 5)
	for i := 0; i < 4; i++ {
		worker := &ChannelsWorker{id: i}
		go worker.process(c)
	}
	for i := 0; i < 110; i++ {
		c <- rand.Int()
		fmt.Printf("channel lenght %d\n", len(c))
		// time.Sleep(time.Millisecond * 10)
	}
}

func (w ChannelsWorker) process(c chan int) {
	for {
		data := <-c
		fmt.Printf("worker %d got %d\n", w.id, data)
	}
}

func channelsSelectExample() {
	c := make(chan int)

	for {
		select {
		case c <- rand.Int():
		default:
			fmt.Println("dropped")
		}
		time.Sleep(time.Millisecond * 50)
	}
}
