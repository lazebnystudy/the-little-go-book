package ch9

import (
	"fmt"
	"time"
)

func GoroutinesCall() {
	fmt.Println(".ex1")
	goroutinesSimpleExample()
	fmt.Println(".ex2")
	exampleWithAnonymous()
}

func goroutinesSimpleExample() {
	fmt.Println("start")
	go processGoroutineExampleSimple()
	time.Sleep(time.Millisecond * 10)
	fmt.Println("done")
}

func processGoroutineExampleSimple() {
	fmt.Println("processing")
}

func exampleWithAnonymous() {
	fmt.Println("start")
	go func() { fmt.Println("processing") }()
	time.Sleep(time.Millisecond * 10)
	fmt.Println("done")
}
