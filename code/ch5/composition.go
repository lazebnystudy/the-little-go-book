package ch5

// It is similar to mixins

import (
	"fmt"
)

type Person struct {
	Name string
}

func (p *Person) Introduce() {
	fmt.Printf("Hi, my name is %s\n", p.Name)
}

type ComposedSaiyan struct {
	*Person
	Power int
}

func CompositionCall() {
	goku := &ComposedSaiyan{
		Person: &Person{Name: "Goku"},
		Power:  1000,
	}
	goku.Introduce()
	fmt.Println(goku.Name)
	fmt.Println(goku.Person.Name)
}
