package ch5

// Golang doesn't support overloading but we can use a trick:

import (
	"fmt"
)

func (s *OverloadedSaiyan) Introduce() {
	fmt.Printf("Hi, my name is %s. Ya!\n", s.Name)
}

type OverloadedSaiyan struct {
	*Person
	Power int
}

func OverloadingCall() {
	goku := &OverloadedSaiyan{
		Person: &Person{Name: "Goku"},
		Power:  1000,
	}
	goku.Introduce()
	goku.Person.Introduce()
}
