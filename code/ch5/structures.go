package ch5

import (
	"fmt"
)

type Saiyan struct {
	Name  string
	Power int
}

func StructuresCall() {
	goku := &Saiyan{Name: "Goku", Power: 9000}
	goku.Super()
	// Super(goku)
	fmt.Println(goku)
}

func Super(s *Saiyan) {
	// s = &Saiyan{Name: "Guku", Power: 1000}
	s.Power += 10000
}

func (s *Saiyan) Super() {
	s.Power += 10000
}
