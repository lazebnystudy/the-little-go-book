package ch4

import (
	"fmt"
)

func VariablesCall() {
	name, power := "Samuel", 10
	fmt.Printf("%s's power is over %d\n", name, power)
}
