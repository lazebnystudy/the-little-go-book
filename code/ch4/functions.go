package ch4

import (
	"fmt"
)

func FunctionsCall() {
	fmt.Println(add(500, 12))
}

func add(a, b int) int {
	return a + b
}
